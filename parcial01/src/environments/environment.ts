// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
      apiKey: "AIzaSyDD0j_pUGsjFa0Kms5gvrSHMel9MT3eUm8",
      authDomain: "apps2-7279c.firebaseapp.com",
      databaseURL: "https://apps2-7279c.firebaseio.com",
      projectId: "apps2-7279c",
      storageBucket: "apps2-7279c.appspot.com",
      messagingSenderId: "267200118979",
      appId: "1:267200118979:web:ae12453f9c699370e76276"
    }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
