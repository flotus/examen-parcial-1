export interface Data {
    id?:string;
    nameofcity:string;
    country:string;
    climate:string;
    qualification:number;
    photo:string;
}
